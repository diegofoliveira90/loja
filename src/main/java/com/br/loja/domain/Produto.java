package com.br.loja.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Produto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String nome;
	private String detalhes;
	private double largura;
	private double altura;
	private double profundidade;
	private double preco;
	
	@OneToMany(mappedBy = "produto_id")
	private List<Imagem> imagens = new ArrayList<Imagem>();
	
	@ManyToOne
	@JoinColumn(name= "fornecedor_id")
	private Fornecedor fornecedor;
	
	@ManyToOne
	@JoinColumn(name= "categoria_id")
	private Categoria categoria;
	
	public Produto() {};
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDetalhes() {
		return detalhes;
	}

	public void setDetalhes(String detalhes) {
		this.detalhes = detalhes;
	}

	public double getLargura() {
		return largura;
	}

	public void setLargura(double largura) {
		this.largura = largura;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getProfundidade() {
		return profundidade;
	}

	public void setProfundidade(double profundidade) {
		this.profundidade = profundidade;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public Produto(String nome, String detalhes, double largura, double altura, double profundidade,
			double preco,Categoria categoria) {
		super();
		this.nome = nome;
		this.detalhes = detalhes;
		this.largura = largura;
		this.altura = altura;
		this.profundidade = profundidade;
		this.preco = preco;
		this.categoria = categoria;
	}
	
}
