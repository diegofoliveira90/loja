package com.br.loja.domain;

import java.util.Date;

import javax.persistence.Entity;

import com.br.loja.domain.enums.EstadoPagamento;

@Entity
public class PagamentoComBoleto extends Pagamento{

	private static final long serialVersionUID = 1L;
	private Date dataVencimento;
	private Date dataPagamento;
	
	public PagamentoComBoleto() {
		
	}

	public PagamentoComBoleto(EstadoPagamento pagamento, Pedido pedido, Cliente cliente, Endereco enderecoEntrega, Date dataVencimento,Date dataPagamento) {
		super(pagamento, pedido, cliente, enderecoEntrega);
		this.dataVencimento = dataVencimento;
		this.dataPagamento = dataPagamento;
	
	}

	public Date getDataVencimento() {
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}
	
}
