package com.br.loja.domain.enums;

public enum TipoCliente {

	PESSOAFISICA(1, "Pessoa Física"),
	PESSOAJURIDICA(2, "Pessoa Juridica");
	
	
	private int cod;
	private String tipo;
	private TipoCliente(int cod, String tipo) {
		this.cod = cod;
		this.tipo = tipo;
	}
	
	public int getCod() {
		return cod;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public static TipoCliente getEnum(Integer cod) {
		if(cod == null) {
			return null;
		}
		
		for(TipoCliente x:TipoCliente.values()) {
			if(x.equals(cod)) {
				return x;
			}
		}
		throw new IllegalArgumentException("id inválido: "+cod);
	}
}
