package com.br.loja.domain.enums;

public enum EstadoPagamento {

	PENDENTE(1, "Pendente"),
	QUITADO(2, "Quitado"),
	CANCELADO(3,"Cancelado");
	
	
	private int cod;
	private String tipo;
	private EstadoPagamento(int cod, String tipo) {
		this.cod = cod;
		this.tipo = tipo;
	}
	
	public int getCod() {
		return cod;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	public static EstadoPagamento getEnum(Integer cod) {
		if(cod == null) {
			return null;
		}
		
		for(EstadoPagamento x:EstadoPagamento.values()) {
			if(x.getCod()==(cod)) {
				return x;
			}
		}
		throw new IllegalArgumentException("id inválido: "+cod);
	}
}
