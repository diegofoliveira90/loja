package com.br.loja.domain.enums;

public enum Perfil {

	ADMIN(1, "ROLE_ADMIN"), 
	CLIENTE(2, "ROLE_CLIENTE"), 
	FORNECEDOR(3, "ROLE_FORNECEDOR"),
	COMPRADOR(4, "ROLE_COMPRADOR");

	private int cod;
	private String tipo;

	private Perfil(int cod, String tipo) {
		this.cod = cod;
		this.tipo = tipo;
	}

	public int getCod() {
		return cod;
	}

	public String getTipo() {
		return tipo;
	}

	public static Perfil getEnum(Integer cod) {
		if (cod == null) {
			return null;
		}

		for (Perfil x : Perfil.values()) {
			if (x.cod == cod) {
				return x;
			}
		}
		throw new IllegalArgumentException("id inválido: " + cod);
	}
}
