package com.br.loja.domain;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;


@Entity
public class Pedido implements Serializable{

	private static final long serialVersionUID = 1L;

	//atributos
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_pedido;
	private Date data_pedido;
	private Time hora_pedido;
	
	@OneToOne(cascade = CascadeType.ALL, mappedBy = "pedido")
	private Pagamento pagamento;
	
	@ManyToOne
	@JoinColumn(name="cliente_id")
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name="endereco_entrega")
	private Endereco enderecoEntrega;
	
	//metodos de acesso
	public int getId_pedido() {
		return id_pedido;
	}
	public void setId_pedido(int id_pedido) {
		this.id_pedido = id_pedido;
	}
	public Date getData_pedido() {
		return data_pedido;
	}
	public void setData_pedido(Date data_pedido) {
		this.data_pedido = data_pedido;
	}
	public Time getHora_pedido() {
		return hora_pedido;
	}
	public void setHora_pedido(Time hora_pedido) {
		this.hora_pedido = hora_pedido;
	}
	
	public Pagamento getPagamento() {
		return pagamento;
	}
	public void setPagamento(Pagamento pagamento) {
		this.pagamento = pagamento;
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Endereco getEnderecoEntrega() {
		return enderecoEntrega;
	}

	public void setEnderecoEntrega(Endereco enderecoEntrega) {
		this.enderecoEntrega = enderecoEntrega;
	}
	
	public Pedido() {
		
	}
	public Pedido(Date data_pedido, Time hora_pedido, Pagamento pagamento) {
		super();
		this.data_pedido = data_pedido;
		this.hora_pedido = hora_pedido;
		this.setPagamento(pagamento);
	}	
}
