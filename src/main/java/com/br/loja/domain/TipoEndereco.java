package com.br.loja.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TipoEndereco implements Serializable{

	private static final long serialVersionUID = 1L;

	//atributos
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_endereco;
	private String tipo;
	
	//metodos de acesso
	public int getId_endereco() {
		return id_endereco;
	}
	public void setId_endereco(int id_endereco) {
		this.id_endereco = id_endereco;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	
	

}
