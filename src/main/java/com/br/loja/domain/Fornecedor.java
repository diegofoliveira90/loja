package com.br.loja.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.br.loja.dto.FornecedorDTO;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Fornecedor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String nome;

	@JsonManagedReference
	@OneToMany(mappedBy = "fornecedor")
	private List<Endereco> enderecos = new ArrayList<Endereco>();

	private String CNPJ;
	private String email;

	@ElementCollection
	@CollectionTable(name = "TELEFONE")
	private Set<String> telefones = new HashSet<>();

	@OneToMany(mappedBy = "fornecedor")
	private List<Produto> produto = new ArrayList<>();

	public Fornecedor() {

	}

	public Fornecedor(String nome, List<Endereco> enderecos, String cNPJ, String email, Set<String> telefones) {

		this.nome = nome;
		this.enderecos = enderecos;
		CNPJ = cNPJ;
		this.email = email;
		this.telefones = telefones;
	}

	public Fornecedor(FornecedorDTO objDTO) {
		this.nome = objDTO.getNome();
		this.enderecos = objDTO.getEndereco();
		this.CNPJ = objDTO.getCnpj();
		this.email = objDTO.getEmail();

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Endereco> getEndereco() {
		return enderecos;
	}

	public void setEndereco(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public String getCNPJ() {
		return CNPJ;
	}

	public void setCNPJ(String cNPJ) {
		CNPJ = cNPJ;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(Set<String> telefones) {
		this.telefones = telefones;
	}

}
