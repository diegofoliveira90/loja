package com.br.loja.domain;

import javax.persistence.Entity;

import com.br.loja.domain.enums.EstadoPagamento;

@Entity
public class PagamentoComCartao extends Pagamento{

	private static final long serialVersionUID = 1L;
	private int numeroParcelas;
	
	public PagamentoComCartao() {
		
	}

	public PagamentoComCartao(EstadoPagamento pagamento, Pedido pedido, Cliente cliente, Endereco enderecoEntrega, int numeroParcelas) {
		super(pagamento, pedido, cliente, enderecoEntrega);
		this.numeroParcelas = numeroParcelas;
	}

	public int getNumeroParcelas() {
		return numeroParcelas;
	}

	public void setNumeroParcelas(int numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}
		
}
