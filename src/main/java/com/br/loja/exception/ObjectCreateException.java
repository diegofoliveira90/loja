package com.br.loja.exception;

public class ObjectCreateException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public ObjectCreateException(String msg) {
		super(msg);		
	}
	
	public ObjectCreateException(String msg, Throwable cause) {
		super(msg,cause);
	}
	
}
