package com.br.loja.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.br.loja.domain.Produto;
import com.br.loja.exception.ObjectNotFoundException;
import com.br.loja.repositories.ProdutoRepository;
import com.br.loja.services.exception.DataIntegrityException;

@Service
public class ProdutoService {
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Produto find(int id) {
		
		Produto obj = produtoRepository.findById(id).get();
		if(obj == null) {
			throw new ObjectNotFoundException("Objeto não encontrado! id: "+ id
					+ " ,Tipo: "+Produto.class.getName());
		}
		return obj;
	}
	
	public Produto insert(Produto obj) {
		
		return produtoRepository.save(obj);
	}
	
	public Produto update(Produto obj) {
		find(obj.getId());	
		return produtoRepository.save(obj);
	}

	public void delete(int id) {
		find(id);
		try {
			produtoRepository.deleteById(id);		
		}catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não e possivel excluir um produto");
		}
	}

	public List<Produto> findAll() {		
		return produtoRepository.findAll();
	}
	
	public Page<Produto> findPage(int page, int linesPerPage, String orderBy, String direction){		
		PageRequest pageRequest = PageRequest.of(page, linesPerPage, Direction.valueOf(direction), orderBy);				
		return produtoRepository.findAll(pageRequest);
	}
}
