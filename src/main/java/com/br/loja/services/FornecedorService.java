package com.br.loja.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.loja.domain.Fornecedor;
import com.br.loja.domain.enums.Perfil;
import com.br.loja.dto.FornecedorDTO;
import com.br.loja.exception.ObjectCreateException;
import com.br.loja.exception.ObjectNotFoundException;
import com.br.loja.repositories.FornecedorRepository;
import com.br.loja.resources.exception.AuthorizationException;
import com.br.loja.security.UserSS;

@Service
public class FornecedorService {

	@Autowired
	private FornecedorRepository fornecedorRepository;

	@Autowired
	private UserService userService;

	public FornecedorDTO buscar(Integer id) {

		UserSS user = userService.authenticated();
		if (user == null || !user.hasRole(Perfil.ADMIN)
				|| !user.hasRole(Perfil.COMPRADOR) && !id.equals(user.getID())) {
			throw new AuthorizationException("Acesso Negado");
		}

		Fornecedor obj = fornecedorRepository.findById(id).get();

		if (obj == null) {
			throw new ObjectNotFoundException(
					"Objeto não encontrado! id: " + id + " ,Tipo: " + Fornecedor.class.getName());
		}

		return new FornecedorDTO(obj);
	}

	public FornecedorDTO cadastrar(FornecedorDTO objDTO) {

		UserSS user = userService.authenticated();
		if (user == null || !user.hasRole(Perfil.ADMIN) || !user.hasRole(Perfil.COMPRADOR)) {
			throw new AuthorizationException("Acesso Negado");
		}

		try {
			Fornecedor resultado = fornecedorRepository.save(new Fornecedor(objDTO));
			return new FornecedorDTO(resultado);

		} catch (Exception e) {
			throw new ObjectCreateException(
					"Nao foi possivel cadastrar o objeto da classe" + Fornecedor.class.getName());
		}

	}

	public FornecedorDTO atualizar(FornecedorDTO objDTO) {

		UserSS user = userService.authenticated();
		if (user == null || !user.hasRole(Perfil.ADMIN) || !user.hasRole(Perfil.COMPRADOR)) {
			throw new AuthorizationException("Acesso Negado");
		}

		try {
			Fornecedor fornecedor = fornecedorRepository.getOne(objDTO.getId());
			updateData(fornecedor, objDTO);
			fornecedorRepository.save(fornecedor);
			return new FornecedorDTO(fornecedor);

		} catch (Exception e) {
			throw new ObjectCreateException(
					"Nao foi possivel atualizar o objeto da classe" + Fornecedor.class.getName());
		}

	}

	private void updateData(Fornecedor entity, FornecedorDTO obj) {

		entity.setNome(obj.getNome());
		entity.setEmail(obj.getEmail());
		entity.setTelefones(obj.getTelefones());
		entity.setEndereco(obj.getEndereco());

	}

}
