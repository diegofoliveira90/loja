package com.br.loja.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.loja.domain.Cliente;
import com.br.loja.dto.ClienteDTO;
import com.br.loja.exception.ObjectNotFoundException;
import com.br.loja.repositories.ClienteRepository;
import com.br.loja.services.exception.CreateException;
import com.br.loja.services.util.AutorizationUtil;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private AutorizationUtil autorizationUtil;

	public ClienteDTO buscar(Integer id) {

		if (autorizationUtil.validaUsuarioProprio(id)) {

			Cliente cliente = clienteRepository.findById(id).get();
			if (cliente == null) {
				throw new ObjectNotFoundException("Objeto não encontrado!, tipo: " + Cliente.class.getName());
			}
			return new ClienteDTO(cliente);

		}
		return null;
	}

	public ClienteDTO Cadastrar(ClienteDTO cliente) {

		try {
			Cliente resultado = clienteRepository.save(new Cliente(cliente));
			return new ClienteDTO(resultado);
		} catch (Exception ex) {
			throw new CreateException("Nao foi possivel " + Cliente.class.getName());
		}
	}

	public Cliente atualizar(Cliente cliente) {

		return null;
	}
}
