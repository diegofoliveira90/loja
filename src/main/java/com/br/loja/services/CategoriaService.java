package com.br.loja.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.br.loja.domain.Categoria;
import com.br.loja.domain.Produto;
import com.br.loja.dto.CategoriaDTO;
import com.br.loja.exception.ObjectNotFoundException;
import com.br.loja.repositories.CategoriaRepository;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepo;
	
	public List<CategoriaDTO> getAll(){
		try {			
			List<CategoriaDTO> lista = categoriaRepo.findAll().stream().map(obj -> new CategoriaDTO(obj,"")).collect(Collectors.toList()); 
			return lista;
		}catch (Exception e) {
			throw new ObjectNotFoundException("Objeto não encontrado! "
					+ " ,Tipo: "+Produto.class.getName());
		}
	}
	
	public CategoriaDTO insert(CategoriaDTO categoria) {
		
		try {
			Categoria cat = new Categoria();
			cat = categoriaRepo.save(cat);
			categoria = new CategoriaDTO(cat,"");
			return categoria;
					
		}catch(Exception e) {
			throw new ObjectNotFoundException("Objeto não encontrado! "
					+ " ,Tipo: "+Produto.class.getName());
		}
		
	}
}
