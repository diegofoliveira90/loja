package com.br.loja.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.br.loja.domain.Usuario;
import com.br.loja.repositories.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepo;
	
	@Autowired
	private BCryptPasswordEncoder bp;
	
	public Usuario login(String usuario, String senha) {
		
		Usuario user = usuarioRepo.findByUsuario(usuario);
		if(bp.matches(senha, user.getSenha())) {
			return user;
		}		
		return null;
	}
	
	public Usuario findByUser(String usuario) {
		Usuario user = usuarioRepo.findByUsuario(usuario);
		if(user != null) {
			return user;
		}		
		return null;
	}
}
