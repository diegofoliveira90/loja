package com.br.loja.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.br.loja.domain.Usuario;
import com.br.loja.repositories.UsuarioRepository;
import com.br.loja.security.UserSS;

@Service
public class UserDetailServiceImpl implements UserDetailsService{


	@Autowired
	private UsuarioRepository usuarioRepository;
		
	@Override
	public UserDetails loadUserByUsername(String user) throws UsernameNotFoundException {
		
		Usuario usuario = usuarioRepository.findByUsuario(user);
		if(usuario == null) {
			throw new UsernameNotFoundException(user);
		}
		return new UserSS(usuario.getId(),usuario.getUsuario(),usuario.getSenha(),usuario.getPerfis());
	}

}
