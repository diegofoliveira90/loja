package com.br.loja.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.br.loja.domain.Usuario;
import com.br.loja.security.UserSS;

@Service
public class UserService {
	
	@Autowired
	private UsuarioService usuarioService;
	
	public UserSS authenticated() {
		try {		
			
			Object obj =  SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			
			String username = obj.toString();
			
			Usuario usuario = usuarioService.findByUser(username);
			if(usuario == null) {
				throw new UsernameNotFoundException(username);
			}
			return new UserSS(usuario.getId(),usuario.getUsuario(),usuario.getSenha(),usuario.getPerfis());
			
		}catch(Exception e) {
			return null;
		}		
	}
}
