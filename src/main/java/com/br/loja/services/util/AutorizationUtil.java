package com.br.loja.services.util;

import org.springframework.beans.factory.annotation.Autowired;

import com.br.loja.domain.enums.Perfil;
import com.br.loja.resources.exception.AuthorizationException;
import com.br.loja.security.UserSS;
import com.br.loja.services.UserService;

public class AutorizationUtil {
	
	
	public AutorizationUtil() {
		
	}
	
	@Autowired
	private UserService userService;

	public Boolean validaUsuarioProprio(Integer id) {

		UserSS user = userService.authenticated();
		if (user == null || !user.hasRole(Perfil.ADMIN) && !id.equals(user.getID())) {
			throw new AuthorizationException("Acesso Negado");
		}
		
		return true;

	}

}
