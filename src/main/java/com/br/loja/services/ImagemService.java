package com.br.loja.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.br.loja.domain.Imagem;
import com.br.loja.domain.Produto;
import com.br.loja.repositories.ImagemRepository;
import com.br.loja.repositories.ProdutoRepository;
import com.br.loja.services.exception.FileStorageException;
import com.br.loja.services.exception.MyFileNotFoundException;

@Service
public class ImagemService {

	@Autowired
	private ImagemRepository imagemRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Imagem armazenaImagem(MultipartFile arquivo,Integer produtoId) {
		
		Produto produto = produtoRepository.findById(produtoId).get();
		
		 // Normalize file name
        String fileName = StringUtils.cleanPath(arquivo.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("O nome do arquivo contem caracteres invalidos: " + fileName);
            }
            
            Imagem img = new Imagem(fileName,arquivo.getBytes(),produto,arquivo.getContentType());

            return imagemRepository.save(img);
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
		
	}
	
	public Imagem recuperaImagem(Integer id) {
		return imagemRepository.findById(id)
				.orElseThrow(() -> new MyFileNotFoundException("Arquivo não encontrado pelo id "+id));
	}
}
