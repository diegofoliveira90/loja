package com.br.loja.services.exception;

public class CreateException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public CreateException(String msg) {
		super(msg);
	}
	
	public CreateException(String msg,Throwable cause) {
		super(msg,cause);
	}
}
