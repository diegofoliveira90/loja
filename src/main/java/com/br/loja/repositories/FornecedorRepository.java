package com.br.loja.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.loja.domain.Fornecedor;

public interface FornecedorRepository extends JpaRepository<Fornecedor, Integer> {

}
