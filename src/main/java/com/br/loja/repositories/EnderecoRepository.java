package com.br.loja.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.loja.domain.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Integer> {

}
