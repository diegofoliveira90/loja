package com.br.loja.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.loja.domain.Imagem;

public interface ImagemRepository extends JpaRepository<Imagem, Integer>{

}
