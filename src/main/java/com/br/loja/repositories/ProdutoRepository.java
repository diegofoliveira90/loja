package com.br.loja.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.loja.domain.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Integer>{

}
