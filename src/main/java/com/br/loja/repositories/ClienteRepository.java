package com.br.loja.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.loja.domain.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer>{

}
