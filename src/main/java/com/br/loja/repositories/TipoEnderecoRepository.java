package com.br.loja.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.loja.domain.TipoEndereco;

public interface TipoEnderecoRepository extends JpaRepository<TipoEndereco, Integer> {

}
