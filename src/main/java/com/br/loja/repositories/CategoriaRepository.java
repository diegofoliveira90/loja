package com.br.loja.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.loja.domain.Categoria;

public interface CategoriaRepository extends JpaRepository<Categoria, Integer>{

}
