package com.br.loja.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.loja.domain.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Integer>{

}
