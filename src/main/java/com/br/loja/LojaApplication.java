package com.br.loja;

import java.util.Arrays;
import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.br.loja.domain.Categoria;
import com.br.loja.domain.Cliente;
import com.br.loja.domain.Endereco;
import com.br.loja.domain.Fornecedor;
import com.br.loja.domain.Produto;
import com.br.loja.domain.Usuario;
import com.br.loja.domain.enums.Perfil;
import com.br.loja.repositories.CategoriaRepository;
import com.br.loja.repositories.ClienteRepository;
import com.br.loja.repositories.EnderecoRepository;
import com.br.loja.repositories.FornecedorRepository;
import com.br.loja.repositories.ProdutoRepository;
import com.br.loja.repositories.UsuarioRepository;

@SpringBootApplication
//public class LojaApplication extends SpringBootServletInitializer {
public class LojaApplication implements CommandLineRunner {
	@Autowired
	private ClienteRepository clienteRepo;

	@Autowired
	private EnderecoRepository enderecoRepo;

	@Autowired
	private ProdutoRepository produtoRepo;

	@Autowired
	private UsuarioRepository usuarioRepo;

	@Autowired
	private BCryptPasswordEncoder be;

	@Autowired
	private CategoriaRepository categoriaRepo;

	@Autowired
	private FornecedorRepository fornecedorRepo;

	@PostConstruct
	void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("Hora oficial do Brasil"));
	}

	public static void main(String[] args) {
		SpringApplication.run(LojaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		Cliente c1 = new Cliente(null, "Diego Oliveira", "392.460.478-93");
		Cliente c2 = new Cliente(null, "Thiago Oliveira", "399.466.555.95");

		c1 = clienteRepo.save(c1);
		c2 = clienteRepo.save(c2);

		Usuario u1 = new Usuario("diego", be.encode("123"), c1, Perfil.ADMIN);
		Usuario u2 = new Usuario("thiago", be.encode("321"), c2, Perfil.ADMIN);

		usuarioRepo.save(u1);
		usuarioRepo.save(u2);

		Fornecedor f1 = new Fornecedor("teste", null, "2387462384", "teste@gmail.com", null);

		fornecedorRepo.save(f1);

		Endereco e1 = new Endereco("rua jose1", 123, "complemento", "bairro nosso", "São Paulo", "Brasil", 13057400,
				true, c1, null);
		Endereco e2 = new Endereco("rua jose1", 321, "complemento", "bairro nosso", "São Paulo", "Brasil", 13057400,
				true, c2, null);

		Endereco e3 = new Endereco("rua jose1", 321, "complemento", "bairro nosso", "São Paulo", "Brasil", 13057400,
				true, null, f1);

		e1 = enderecoRepo.save(e1);
		e2 = enderecoRepo.save(e2);
		e3 = enderecoRepo.save(e3);

		Categoria cat1 = new Categoria("Horizontal");
		Categoria cat2 = new Categoria("vertical");
		Categoria cat3 = new Categoria("Rolo");
		Categoria cat4 = new Categoria("Romana");
		cat1 = categoriaRepo.save(cat1);
		cat2 = categoriaRepo.save(cat2);
		cat3 = categoriaRepo.save(cat3);
		cat4 = categoriaRepo.save(cat4);

		Produto p1 = new Produto("Persiana 1", "persiana 1", 3.2, 4.5, 0.3, 35.00, cat1);
		Produto p2 = new Produto("Persiana 2", "persiana 2", 3.0, 4.5, 0.3, 40.00, cat2);
		Produto p3 = new Produto("Persiana 3", "persiana 3", 4.0, 4.5, 0.3, 25.00, cat2);
		Produto p4 = new Produto("Persiana 4", "persiana 4", 2.2, 4.5, 0.3, 35.00, cat4);
		Produto p5 = new Produto("Persiana 5", "persiana 5", 2.2, 4.5, 0.3, 35.00, cat2);
		Produto p6 = new Produto("Persiana 6", "persiana 6", 2.2, 4.5, 0.3, 35.00, cat2);
		Produto p7 = new Produto("Persiana 7", "persiana 7", 2.2, 4.5, 0.3, 35.00, cat3);
		Produto p8 = new Produto("Persiana 8", "persiana 8", 2.2, 4.5, 0.3, 35.00, cat2);
		Produto p9 = new Produto("Persiana 9", "persiana 9", 2.2, 4.5, 0.3, 35.00, cat1);
		Produto p10 = new Produto("Persiana 10", "persiana 10", 2.2, 4.5, 0.3, 35.00, cat3);

		produtoRepo.saveAll(Arrays.asList(p1, p2, p3, p4, p5, p6, p7, p8, p9, p10));

	}

}
