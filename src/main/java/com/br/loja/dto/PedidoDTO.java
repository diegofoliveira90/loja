package com.br.loja.dto;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import com.br.loja.domain.Pedido;

public class PedidoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id_pedido;
	private Date data_pedido;
	private Time hora_pedido;

	public PedidoDTO() {

	}

	public PedidoDTO(Pedido obj) {
		this.id_pedido = obj.getId_pedido();
		this.data_pedido = obj.getData_pedido();
		this.hora_pedido = obj.getHora_pedido();

	}

	public int getId_pedido() {
		return id_pedido;
	}

	public void setId_pedido(int id_pedido) {
		this.id_pedido = id_pedido;
	}

	public Date getData_pedido() {
		return data_pedido;
	}

	public void setData_pedido(Date data_pedido) {
		this.data_pedido = data_pedido;
	}

	public Time getHora_pedido() {
		return hora_pedido;
	}

	public void setHora_pedido(Time hora_pedido) {
		this.hora_pedido = hora_pedido;
	}

}
