package com.br.loja.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import com.br.loja.domain.Cliente;
import com.br.loja.domain.Endereco;
import com.br.loja.domain.Pedido;

public class ClienteDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String nome;
	private String cpfOuCnpj;
	private List<Endereco> endereco;
	private Set<String> telefones;
	private List<Pedido> pedidos;

	public ClienteDTO() {
		super();
	}

	public ClienteDTO(Cliente obj) {

		this.id = obj.getId();
		this.nome = obj.getNome();
		this.cpfOuCnpj = obj.getCpfOuCnpj();
		this.endereco = obj.getEnderecos();
		this.telefones = obj.getTelefones();
		this.pedidos = obj.getPedidos();

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpfOuCnpj() {
		return cpfOuCnpj;
	}

	public void setCpfOuCnpj(String cpfOuCnpj) {
		this.cpfOuCnpj = cpfOuCnpj;
	}

	public List<Endereco> getEndereco() {
		return endereco;
	}

	public void setEndereco(List<Endereco> endereco) {
		this.endereco = endereco;
	}

	public Set<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(Set<String> telefones) {
		this.telefones = telefones;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

}
