package com.br.loja.dto;

import java.io.Serializable;

import com.br.loja.domain.Categoria;

public class CategoriaDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String Nome;
	private String url;

	public CategoriaDTO() {

	}

	public CategoriaDTO(Categoria categoria, String url) {
		super();
		Nome = categoria.getNome();
		this.url = url;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
