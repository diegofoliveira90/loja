package com.br.loja.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.loja.dto.CategoriaDTO;
import com.br.loja.services.CategoriaService;

@RestController
@RequestMapping(value = "/categoria")
public class CategoriaResources {

	@Autowired
	private CategoriaService categoriaSerivce;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<?> Listar() {
		List<CategoriaDTO> obj = categoriaSerivce.getAll();
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> insert(@RequestBody CategoriaDTO categoria){		
		return ResponseEntity.ok().body(categoriaSerivce.insert(categoria));
	}
}
