package com.br.loja.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.loja.domain.Usuario;
import com.br.loja.dto.CredenciaisDTO;
import com.br.loja.services.UsuarioService;

@RestController
@RequestMapping(value="/usuario")
public class UsuarioRessource {

	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> Logar(@RequestBody CredenciaisDTO credenciais){
		Usuario user = usuarioService.login(credenciais.getUsuario(), credenciais.getSenha());
		
		if(user != null) {
			return ResponseEntity.ok().body(user);
		}
		else {
			return ((BodyBuilder) ResponseEntity.notFound()).body("usuario não encontrado");
		}
		
	}
}
