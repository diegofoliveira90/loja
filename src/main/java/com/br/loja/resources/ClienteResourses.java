package com.br.loja.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.loja.dto.ClienteDTO;
import com.br.loja.services.ClienteService;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteResourses {

	@Autowired
	private ClienteService clienteService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<ClienteDTO> buscar(@PathVariable int id) {
		return ResponseEntity.ok().body(clienteService.buscar(id));
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<ClienteDTO> inserir(@RequestBody ClienteDTO cliente) {
		return ResponseEntity.ok().body(clienteService.Cadastrar(cliente));
	}
	
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<ClienteDTO> atualizar(@RequestBody ClienteDTO cliente) {
		return ResponseEntity.ok().body(clienteService.Cadastrar(cliente));
	}
	

}
