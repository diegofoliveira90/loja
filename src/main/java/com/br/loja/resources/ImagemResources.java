package com.br.loja.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.br.loja.domain.Imagem;
import com.br.loja.services.ImagemService;

@RestController
@RequestMapping(value = "/imagem")
public class ImagemResources {

	@Autowired
	private ImagemService imagemService;
		
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> inserir(@RequestParam("file") MultipartFile file,int produtoId){
		
		return ResponseEntity.ok().body(imagemService.armazenaImagem(file, produtoId));
		
	}
	
	@RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> downloadFile(@PathVariable int fileId) {
        
        Imagem image = imagemService.recuperaImagem(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(image.getTipo()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + image.getNome() + "\"")
                .body(new ByteArrayResource(image.getData()));
    }

}
