package com.br.loja.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.br.loja.dto.FornecedorDTO;
import com.br.loja.services.FornecedorService;

@RestController
@RequestMapping(value = "/fornecedor")
public class FornecedorResourses {

	@Autowired
	private FornecedorService fornecedorService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> buscar(@PathVariable int id) {
		return ResponseEntity.ok().body(fornecedorService.buscar(id));
	}

	@PreAuthorize("hasAnyRole('ADMIN', 'COMPRADOR')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> cadastrar(@RequestBody FornecedorDTO objDTO) {
		return ResponseEntity.ok().body(fornecedorService.cadastrar(objDTO));

	}

	@PreAuthorize("hasAnyRole('ADMIN', 'COMPRADOR')")
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> atualizar(@RequestBody FornecedorDTO objDTO) {
		return ResponseEntity.ok().body(fornecedorService.atualizar(objDTO));

	}

}
